(function main(global) {
	requirejs.config({
		paths: {
			threejs: '../lib/three',
			underscore: '../lib/underscore',
			MaskPass: '../lib/three/MaskPass',
			CopyShader: '../lib/three/CopyShader',
			EffectComposer: '../lib/three/EffectComposer',
			DotScreenShader: '../lib/three/DotScreenShader',
			RenderPass: '../lib/three/RenderPass',
			ShaderPass: '../lib/three/ShaderPass'
		},
		deps: ['app'],
		callback: function(app) {
			app.run();
		}
	});
})(this);