define('linedcube', ['threejs', 'underscore'], function(threejs, underscore) {

	var getGeometry = function(v1, v2) {
		var geometry = new THREE.Geometry();
		geometry.vertices.push(v1, v2);
		return geometry;
	};

	THREE.LinedCube = function(x, y, z, parameters) {
		THREE.Group.call(this);
		
		if (parameters === undefined) {
			parameters = {color: 0x000000};
		}
		
		var material = new THREE.LineBasicMaterial({
			color: parameters.color
		});	
		
		this.add(new THREE.Line(getGeometry(new THREE.Vector3(-x / 2, -y / 2, -z / 2), new THREE.Vector3( x / 2, -y / 2, -z / 2)), material));
		this.add(new THREE.Line(getGeometry(new THREE.Vector3(-x / 2, -y / 2, -z / 2), new THREE.Vector3(-x / 2,  y / 2, -z / 2)), material));
		this.add(new THREE.Line(getGeometry(new THREE.Vector3(-x / 2, -y / 2, -z / 2), new THREE.Vector3(-x / 2, -y / 2,  z / 2)), material));
		
		this.add(new THREE.Line(getGeometry(new THREE.Vector3(-x / 2,  y / 2,  z / 2), new THREE.Vector3(-x / 2,  y / 2, -z / 2)), material));
		this.add(new THREE.Line(getGeometry(new THREE.Vector3(-x / 2,  y / 2,  z / 2), new THREE.Vector3(-x / 2, -y / 2,  z / 2)), material));
		this.add(new THREE.Line(getGeometry(new THREE.Vector3(-x / 2,  y / 2,  z / 2), new THREE.Vector3( x / 2,  y / 2,  z / 2)), material));
		
		this.add(new THREE.Line(getGeometry(new THREE.Vector3( x / 2,  y / 2, -z / 2), new THREE.Vector3(-x / 2,  y / 2, -z / 2)), material));
		this.add(new THREE.Line(getGeometry(new THREE.Vector3( x / 2,  y / 2, -z / 2), new THREE.Vector3( x / 2, -y / 2, -z / 2)), material));
		this.add(new THREE.Line(getGeometry(new THREE.Vector3( x / 2,  y / 2, -z / 2), new THREE.Vector3( x / 2,  y / 2,  z / 2)), material));
		
		this.add(new THREE.Line(getGeometry(new THREE.Vector3( x / 2, -y / 2,  z / 2), new THREE.Vector3( x / 2,  y / 2,  z / 2)), material));
		this.add(new THREE.Line(getGeometry(new THREE.Vector3( x / 2, -y / 2,  z / 2), new THREE.Vector3(-x / 2, -y / 2,  z / 2)), material));
		this.add(new THREE.Line(getGeometry(new THREE.Vector3( x / 2, -y / 2,  z / 2), new THREE.Vector3( x / 2, -y / 2, -z / 2)), material));
	};
	
	THREE.LinedCube.prototype = new THREE.Group();

	return null;
});