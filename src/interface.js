define('interface', [], function() {

	var Interface = function(){
		this.init();
	};
	
	Interface.prototype = {
		
		container: null,
		
		createButtons: function(container) {
			var btn_add = document.createElement('button');
			btn_add.className = 'btn';
			
			var icon = document.createElement('span');
			icon.className = 'glyphicon glyphicon-plus';	
			btn_add.appendChild(icon);
			
			container.appendChild(btn_add);
		},
		getContainer: function() {
			return this.container;
		},
		init: function() {
			this.container = document.createElement('div');
			this.container.className = 'canvas';
			document.body.appendChild(this.container);
			var panel = document.createElement('div');
			panel.className = 'panel';
			this.container.appendChild(panel);
			this.createButtons(panel);
		}
	};
	
	return new Interface();
});