define('app', ['threejs', 'interface', 'scene'], function(threejs, _interface, scene) {

	var App = function(){};
	
	App.prototype = {
		run: function() {
			scene.createRenderer(_interface.getContainer());
			scene.start();
		}
	}
	
	return new App();
});