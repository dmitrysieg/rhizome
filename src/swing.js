define('swing', ['threejs'], function(threejs) {
	
	var SwingHandler = function(speed, angle){
		this.speed = speed;
		this.angle = angle;
	};
	
	SwingHandler.prototype = {
		handle: function(object, delta) {
			if (this.initialRotation === undefined) {
				this.initialRotation = object.rotation.y;
			}
			
			if (this.swingTime === undefined) {
				this.swingTime = 0.0;
			} else {
				this.swingTime += delta / 1000.0;				
			}
			if (this.swingTime > (60.0 / this.speed)) {
				this.swingTime = 0.0;
			}				
			
			var swingPercent = this.swingTime * this.speed / 60.0;
							
			object.rotation.y = this.initialRotation + this.angle * Math.sin(swingPercent * 2.0 * Math.PI);
		}
	};
	
	var SwingHandlerFactory = function(){};
	
	SwingHandlerFactory.prototype = {
		createSwingHandler: function(speed, angle) {
			return new SwingHandler(speed, angle);
		}
	};
	
	return new SwingHandlerFactory();
});