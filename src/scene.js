define('scene', ['threejs', 'underscore', 'swing', 'linedcube', 'shaders'], function(threejs, underscore, swing, linedcube, shaders) {

	var Scene = function(){};
	
	Scene.prototype = {

		bunch: {
			objects: []
		},
		last_time: 0.0,

		doAnimate: function(delta) {

			for (var i = 0, l = this.bunch.objects.length; i < l; i++) {
				this.bunch.objects[i].swingHandler.handle(this.bunch.objects[i], delta);
			}
		},
		createRenderer: function(container) {

			this.scene = new THREE.Scene();
			this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
			this.camera.position.z = 5;
			
			this.renderer = new THREE.WebGLRenderer();
			this.renderer.setSize(window.innerWidth, window.innerHeight);
			this.renderer.setClearColor(0xF8ECC2, 1);
			this.renderer.shadowMapEnabled = true;
			this.renderer.shadowMapSoft = true;
			container.appendChild(this.renderer.domElement);

			// postprocessing
			this.composer = new THREE.EffectComposer(this.renderer);
			this.composer.addPass(new THREE.RenderPass(this.scene, this.camera));

			var shader = new THREE.ShaderPass(shaders.MyShader);
			shader.renderToScreen = true;
			this.composer.addPass(shader);

			this.renderer.domElement.addEventListener('click', _.bind(this.onClick, this), false);
		},
		
		onClick: function(event) {
	
			console.log("client: " + event.clientX + "; " + event.clientY);	
			
			var mouse = new THREE.Vector3();
			mouse.set(
				(event.clientX / event.target.clientWidth) * 2 - 1,
				- (event.clientY / event.target.clientHeight) * 2 + 1,
				0.5
			);
			mouse.unproject(this.camera);
			
			var dir = mouse.sub(this.camera.position).normalize();
			var distance = - this.camera.position.z / dir.z;
			var pos = this.camera.position.clone().add(dir.multiplyScalar(20.0));
			
			var cube = new THREE.LinedCube(5.0, 3.0, 2.0);
			this.scene.add(cube);
			this.bunch.objects.push(cube);
			cube.swingHandler = swing.createSwingHandler(5.0, 5.0 * (2.0 * Math.PI / 360.0));
			cube.position.set(pos.x, pos.y, pos.z);
			
			console.log("cube: " + cube.position.x + "; " + cube.position.y + "; " + cube.position.z);
		},
		
		render: function(time) {

			this.doAnimate(time - this.last_time);
			this.composer.render(this.scene, this.camera);
			
			this.last_time = time;
			window.requestAnimationFrame(this._render);
		},
		start: function() {
			this._render = _.bind(this.render, this);
			window.requestAnimationFrame(this._render);
		}
	};

	return new Scene();
});
